# Greece Summit Expenses

## What is covered?

* Food and Beverage- the hotel is all inclusive and any time we are not at the hotel we will have food and drinks pre arranged. If you chose not to eat what we or the hotel have provided already, you will be accountable for those expenses. 

* Lodging- All lodging during the dates of the Summit. If you have elected to have a single room, you will be responsible for the additional fee for the single room. 

* Transportation to and from the airport. 

* All Visa costs to get to the summit.

* GitLab sponsored entertainment/activities (for all team members, SO's etc. )

* Travel insurance which has been arranged by GitLab.

## What is not covered?

* Souvenirs

* Free time activities that are not company sponsored

* Food and beverage not included in the all inclusive hotel package.

* If you are brining your child there will be an additional fee.

## How to pay?

* When with the group, we will put expenses on the company card.

* When on your own

    * GitLab employee - submit an expense report via Expensify

    * GitLab contractor - submit an invoice for reimbursement

    * GitLab core team - same as contractors

### Note: Receipts must be included for all reimbursement of $25 or higher

## International Phone Plans

* Please plan on using hotel wifi for service