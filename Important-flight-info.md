##### Q: What airport should we fly in to?

HER > Heraklion Airport. We're working on arranging a transfer from there.

##### Q: If you have a layover somewhere in Europe, do you go through customs at the first place (i.e. if we connect in Zurich and then on to Greece) or in Greece?

Passport control yes - at your entry to the Schengen zone - customs (after you pick up your bags) not until your destination. 
Make sure to check if you need to collect and re-check your bags on your layover, or if they get labelled to your final destination!

##### Q: If you have a long layover at night somewhere can you expense the stay at an airport hotel?

Absolutely! Just keep the spending company money as though it were your own policy in mind when booking your arrangements. 

##### Q: What about wanting to travel after the summit; how does that work with booking a flight that GitLab pays for?

First, think about what the handbook tells us about spending, we depend on you to use good judgement and spend company money as if it were your own.  
Start by  pricing a round trip ticket to and from your home airport and Greece.  
Make sure you are looking for the best deal based on a reasonable flight(s).  
Next, see what a multi city ticket to Greece and return home from your final destination will be. 
If the combined one way flights don’t cost more than the round trip,go for it.

##### Q: Where do I add my flight details and room preference?

[In this sheet](https://docs.google.com/spreadsheets/d/1SO9hpyt5mFmCV2negTicYum4iT7ebUlcGvszSnEX7UY/edit?ts=592dbc06#gid=0) by leaving a comment in the matching cell

##### Q: Do I have to pay for checking a bag for my flight?

GitLab will pay for your flight to include 1 checked bag. That means if you fly to Europe with 1 backpack and a carry on but have to check that bag on your flight onward to Greece, the costs will be covered by GitLab.
If you're flying with a carry on suitcase, a backpack and are checking a bag, which means having to check in 2 suitcases on your flight to Greece (since most airlines only allow 1 carry on) GitLab will pay for the first checked bag and you wil have to pay for the second checked bag.
